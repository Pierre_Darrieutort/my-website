const $ = require('jquery')

$(document).ready(function () {

    //! Carousel interactif
    //? Calcul de la hauteur dynamiquement en fonction du contenu du bloc
    var hauteur = $('#a-propos .wrapperDroite > div').map(function() {
        return $(this).outerHeight()
    }).get()
    var maxHeight = Math.max.apply(null, hauteur)

    $('#a-propos .wrapperGauche').css({
        "height" : maxHeight
    })

    //? État css initial
    $('#a-propos .wrapperDroite > div:not(.active)').css({
                                                            'transform': 'translateY(-50%) scale(0)',
                                                            'z-index': '-1',
                                                            'user-select': 'none'
                                                        })

    $('#a-propos .wrapperGauche h3').mouseover(function () {
        
        //? Changement de la classe 'active'
        $('#a-propos .wrapperGauche').find('.active').removeClass('active')
        $(this).addClass('active')
        $('#a-propos .wrapperDroite').find('.active').removeClass('active')
        var slideData = '#a-propos .wrapperDroite [data-slide="' + $(this).data('slide') + '"]'
        $(slideData).addClass('active')

        //? État css final
        $('#a-propos .wrapperDroite > div:not(.active)').css({
                                                                'transform': 'translateY(-50%) scale(0)',
                                                                'z-index': '-1',
                                                                'user-select': 'none'
                                                            })
        $('#a-propos .wrapperDroite > div.active').css({
                                                                'transform': 'translateY(-50%) scale(1)',
                                                                'z-index': '2'
                                                            })
    })


    //! Contact-modal
    //? Click function to show the Modal
    $(".show, #topContactButton").on("click", function(){
        $(".mask").addClass("active")
    });
    
    //? Function to close the Modal
    function closeModal(){
        $(".mask").removeClass("active")
    }
    
    //? Call the closeModal function on the clicks/keyboard
    $(".close, .mask").on("click", function(){
        closeModal()
    })
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {//? Keycode 'esc'
        closeModal()
        }
    })


    //! recup_mail_contact
    $('.show').on('click', function() {
        var grabMail = $( "#mailContact" ).val()
        $("#mail").val(grabMail)
    })


    //! smooth-scrolling
    $('.js-scrollTo').on('click', function() { // Au clic sur un élément
        var page = $(this).attr('href') // Page cible
        var speed = 750 // Durée de l'animation (en ms)
        $('html, body').animate( { scrollTop: $(page).offset().top }, speed ) // Go
        return false
    })


    //! rotating-text (below the title)
    var txt = document.querySelector(".aok"),
	    max = document.querySelectorAll(".bip").length,
	    pos = 0,
        height = Math.round(txt.offsetHeight / max)
    // txt.parentNode.style.width = txt.offsetWidth + "px"

    setInterval(function(){
        pos += height;
        (pos > height * (max-1)) && (pos = 0)

        txt.style.bottom = -pos + "px"
    }, 1500)


    //! Download button
    $(".btn-circle-download, .download-CV-Text, .telecharger-cv").click(function() {
        $(this).addClass("load")
        setTimeout(function() {
          $(".btn-circle-download").addClass("done")
        }, 1000)
        setTimeout(function() {
          $(".btn-circle-download").removeClass("load done")
        }, 5000)
      });


    //! projet-modal
    //? Click function to show the Modal
    $(".desc-projet, .img-projet").on("click", function(){
        var projetData = '.workContainer[data-projet="' + $(this).parents('.workContainer').data('projet') + '"] '
        $(projetData + ".maskP").addClass("active")
    })
    
    //? Function to close the Modal
    function closeModalP(){
        $(".maskP").removeClass("active")
    }
    
    //? Call the closeModal function on the clicks/keyboard
    $(".closeP, .maskP").on("click", function(){
        closeModalP()
    })
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {//? Keycode 'esc'
        closeModalP()
        }
    })

    //! Remove 'transition' property after end to can apply position: 'fixed' on childs
    var element = $(".modalP")
    element.on('transitionend', function(){
        $(this).css('transform', 'none')
    })


})
