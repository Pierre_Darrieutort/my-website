const path = require('path')
const dev = process.env.NODE_ENV === 'dev'
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

let cssLoaders = [{
    loader: 'css-loader',
    options: {
        importLoaders: 1
    }
}]

if (!dev) {
    cssLoaders.push({
        loader: 'postcss-loader',
        options: {
            plugins: (loader) => [
                require('autoprefixer')({
                    browsers: [
                        ">0.25%",
                        "not ie 11",
                        "not op_mini all"
                    ]
                })
            ]
        }
    })
}

let config = {
    stats: {
        children: dev ? false : true //? Hides overfull of verbose in 'dev' mode
    },
    devServer: {
        stats: 'minimal' //? Hides overfull of verbose using webpack-dev-server
    },
    entry: {
        app: ['./assets/css/app.sass', './assets/js/app.js']
    },
    watch: dev,
    output: {
        path: path.resolve('./dist'),
        filename: dev ? './js/[name].js' : './js/[name].[hash:8].js',
    },
    resolve: {
        alias: {
            '@css': path.resolve('./assets/css'),
            '@js': path.resolve('./assets/js')
        }
    },
    devtool: dev ? 'cheap-module-eval-source-map' : false,
    module: {
        rules: [
            // {
            //     enforce: 'pre',
            //     test: /\.js$/,
            //     exclude: /(node_modules|bower_components)/,
            //     use: ['eslint-loader']
            // },
            // {
            //     test: /\.js$/,
            //     exclude: /(node_modules|bower_components)/,
            //     use: ['babel-loader']
            // },
            {
                test: /\.(sa|sc|c)ss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../' //? Relative path to style's URLs
                        }
                    },
                    ...cssLoaders,
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(svg)$/i,
                loader: 'file-loader',
                options: {
                    outputPath: './img'
                }
            }, //? Above is the svg loader, to transform svg in base64 comment it and add below in 'test' -> svg
            {
                test: /\.(png|jpg|gif)$/i,
                use: [{
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: '[name].[hash:7].[ext]',
                            outputPath: 'img/'
                        }
                    },
                    {
                        loader: 'img-loader',
                        options: {
                            enabled: !dev
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: dev ? './css/[name].css' : './css/[name].[contenthash:8].css',
            disable: dev
        }),
        new OptimizeCssAssetsPlugin({
            cssProcessorPluginOptions: {
                preset: ['default', {
                    discardComments: {
                        removeAll: true
                    }
                }],
            }
        }),
        new CopyWebpackPlugin([
            {
                from: 'assets/docs/*.pdf',
                to: './docs/',
                flatten: true,
                toType: 'dir'
            }
        ]),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './assets/index.html'
        }),
        new HtmlWebpackPlugin({
            filename: './docs/mentions-legales.html',
            template: './assets/docs/mentions-legales.html'
        })
    ]
}
if (!dev) {
    config.plugins.push(new ManifestPlugin())
    config.plugins.push(new CleanWebpackPlugin(['dist/'], {
        exclude: ['contact.php'],
        root: path.resolve('./'),
        verbose: true,
        dry: false
    }))
}

module.exports = config
